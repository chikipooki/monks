import {PageInfo, PageObserver, PageService} from "../models/pagination.models";
import {Component} from "../models/application.models";

export class StepDescriptionComponent extends Component implements PageObserver {

    onPageChanged(info: PageInfo, index: number): void {
        const sd = document.getElementById('step-description')
        if (sd) {
            sd.classList.remove('fade-in')
            sd.classList.add('fade-out')
            setTimeout(() => {
                sd.innerHTML = info.orderNumber
                    ? `<div class="step-desc">Step ${info.orderNumber} out of 8 on the path to digital enlightment</div>`
                    : ''
                sd.classList.remove('fade-out')
                sd.classList.add('fade-in')
            }, 1000)
        }
    }

    render(): string {
        PageService.register(this)
        return `<div id="step-description" class="fade-out"></div>`;
    }

}
