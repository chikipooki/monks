import {Component} from "../models/application.models";

export class LoadingPageComponent extends Component {

  removeExclamationMark() {
    const patience = document.getElementById('patience')
    patience.innerHTML = 'Patience'
  }

  render(): string {
    setTimeout(() => this.removeExclamationMark(), 1000)
    return `
      <div class="loading-page">
        <div class="loading-page__wrapper">
          <div class="loading-page__wrapper__cloud"></div>
          <img class="loading-page__wrapper__monk" src="./src/assets/images/monk.png" alt="monk">
        </div>
        <div class="loading-page__text">
          <div class="loading-page__text__holder">
            <div id="patience" class="loading-page__text--patience">
              Patience!
            </div>
            <div class="overflow-hidden">
              <div class="loading-page__text--padawan">
                , young padawan...
              </div>
            </div>
          </div>
        </div>
      </div>
    `
  }
}
