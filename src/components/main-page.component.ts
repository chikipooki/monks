import {Component} from "../models/application.models";
import {PageInfo, PageObserver, PageService} from "../models/pagination.models";
import {PaginationComponent} from "./pagination.component";
import {StepDescriptionComponent} from "./step-description.component";
import {BigTextComponent} from "./big-text.component";

export class MainPageComponent extends Component implements PageObserver {

  onPageChanged(info: PageInfo, index: number): void {
    const img = document.getElementById('main-page-img')
    if (img) {
      img.style.transform = `translateX(${info.translateX}%)`
    }
  }

  addEventListeners() {
    document.body.addEventListener('click', (e: Event) => {
      const target = e.target as HTMLElement
      if (target.id === 'next-page') {
        PageService.nextPage()
      } else if (target.id === 'prev-page') {
        PageService.prevPage()
      }
    })
    PageService.register(this)
  }

  render(): string {
    return `
    <div id="main" class="main-page">
      <div id="main-page-img-container">
        <img src="./src/assets/images/background.jpg" id="main-page-img">
      </div>
      ${new BigTextComponent().innerHTML}
      <div id="prev-page" class="page-arrow"> < </div>
      <div id="next-page" class="page-arrow"> > </div>
      <div class="left-bottom-corner">
        ${new PaginationComponent().innerHTML}
        ${new StepDescriptionComponent().innerHTML}
      </div>
    </div>
    `
  }
}
