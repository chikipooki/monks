import {Component} from "../models/application.models";
import {PageInfo, PageObserver, PAGES, PageService} from "../models/pagination.models";

export class PaginationComponent extends Component implements PageObserver {

    onPageChanged(info: PageInfo, index: number): void {
        const collection = document.getElementsByClassName('page-link')

        for(let i = 0; i < collection.length; i++) {
            collection.item(i).classList.remove('active-link')
        }

        collection.item(index).classList.add('active-link')
    }

    addEventListeners() {
        PageService.register(this)
        document.body.addEventListener('click', (e: Event) => {
            const target = e.target as HTMLElement
            if (target.classList.contains('page-link')) {
                PageService.requestPage(Number(target.id.split('_')[1]))
            }
        })
    }

    render(): string {
        const pagination = document.createElement('div')
        pagination.id = 'pagination'

        PAGES
            .map((page, index) => {
                const el = document.createElement('div')
                el.id = `page-link_${index}`
                el.className = 'page-link'
                el.innerText = page.orderNumber ? page.orderNumber.toString() : ''
                return el
            })
            .forEach(el => pagination.appendChild(el))

        pagination.children.item(0).classList.add('active-link')

        return pagination.outerHTML;
    }
}
