import {Component} from "../models/application.models";
import {PageInfo, PageObserver, PageService} from "../models/pagination.models";

interface Position {
    top: number,
    left?: number,
    right?: number
}

const BIG_TEXTS: string[][] = [
    ['we are breaking', 'our vow', 'of silence'],
    ['talent is given', 'true skill is', 'earned'],
    ['be flexible to', 'change and', 'sturdy in', 'conviction'],
    ['use many skills', 'yet work as one'],
    ['to master', 'anything find', 'interest in', 'everything'],
    ['individuals', 'flourish', 'if culture', 'and wisdom', 'are shared'],
    ['train for', 'perfection but', 'aim for more'],
    ['take pride in your', 'work but do not', 'seek praise'],
    ['temporary', 'sacrifice brings', 'lasting results'],
    ['become a monk']
]

const TEXT_POSITIONS: Position[] = [
    { top: 3, left: 3, right: undefined},
    { top: 48, left: 3, right: undefined},
    { top: 48, left: 3, right: undefined},
    { top: 48, left: undefined, right: 3},
    { top: 48, left: undefined, right: 3},
    { top: 40, left: undefined, right: 3},
    { top: 48, left: 3, right: undefined},
    { top: 48, left: 3, right: undefined},
    { top: 48, left: 3, right: undefined},
    { top: 5, left: undefined, right: 3}
]

export class BigTextComponent extends Component implements PageObserver {

    onPageChanged(info: PageInfo, index: number): void {
        const bigText = document.getElementById('big-text')
        if (bigText) {
            bigText.classList.remove('fade-in')
            bigText.classList.add('fade-out')
            setTimeout(() => {
                const p = TEXT_POSITIONS[index]
                const texts = BIG_TEXTS[index]
                bigText.innerHTML = `
                    <div class="title" style="position: absolute; left: ${p.left}%; right: ${p.right}%; top: ${p.top}%">
                        ${texts.map(item => '<div class="title__line">' + item + '</div>').join('\n')}
                    </div>
                `
                bigText.classList.remove('fade-out')
                bigText.classList.add('fade-in')
            }, 1000)
        }
    }

    addEventListeners() {
        PageService.register(this)
    }

    render(): string {
        const p = TEXT_POSITIONS[0]
        const texts = BIG_TEXTS[0]
        return `
            <div id="big-text">
                <div class="title" style="position: absolute; left: ${p.left}%; right: ${p.right}%; top: ${p.top}%">
                    ${texts.map(item => '<div class="title__line">' + item + '</div>').join('\n')}
                </div>
            </div>`;
    }
}
