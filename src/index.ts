import { LoadingPageComponent } from './components/loading-page.component'
import { MainPageComponent } from './components/main-page.component'
import './assets/styles/main.sass'
import './assets/styles/loading.sass'
import './assets/styles/pagination.sass'
import './assets/styles/global.sass'

const app = document.getElementById('app')!
app.innerHTML = new LoadingPageComponent().innerHTML

const main = new MainPageComponent()
setTimeout(() => {
    app.innerHTML = main.innerHTML
    setTimeout(() => document.getElementById('main').classList.add('fade-in'), 100)
}, 2000)
