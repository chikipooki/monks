export abstract class Component {

    myElement: HTMLElement

    constructor() {
        this.myElement = document.createElement('div')
        this.myElement.innerHTML = this.render()
        this.addEventListeners()
    }

    addEventListeners() {}

    get innerHTML(): string {
        return this.myElement.innerHTML
    }

    abstract render(): string

}
