class PageObservable {

    private currentPageIndex: number;
    private observers: PageObserver[];

    constructor() {
        this.currentPageIndex = 0
        this.observers = []
    }

    register(observer: PageObserver) {
        this.observers.push(observer)
    }

    unregister(observer: PageObserver) {
        this.observers = this.observers.filter(o => o !== observer)
    }

    requestPage(index: number) {
        if (index < 0 || index >= PAGES.length) {
            return
        }
        this.currentPageIndex = index
        this.observers.forEach(o => o.onPageChanged(this.currentPage, this.currentPageIndex))
    }

    get currentPage(): PageInfo {
        return PAGES[this.currentPageIndex]
    }

    nextPage() {
        this.requestPage(this.currentPageIndex + 1)
    }

    prevPage() {
        this.requestPage(this.currentPageIndex - 1)
    }
}

export const PageService = new PageObservable()

export interface PageObserver {
    onPageChanged(info: PageInfo, index: number): void
}

export interface PageInfo {
    translateX: number;
    orderNumber?: number;
}

export const PAGES: PageInfo[] = [
    {
        translateX: 0
    },
    {
        translateX: -11,
        orderNumber: 1
    },
    {
        translateX: -19,
        orderNumber: 2
    },
    {
        translateX: -31,
        orderNumber: 3
    },
    {
        translateX: -42.5,
        orderNumber: 4
    },
    {
        translateX: -54,
        orderNumber: 5
    },
    {
        translateX: -66.5,
        orderNumber: 6
    },
    {
        translateX: -83,
        orderNumber: 7
    },
    {
        translateX: -83,
        orderNumber: 8
    },
    {
        translateX: -95
    }
]
