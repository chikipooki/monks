const path = require( 'path' );

const ROOT = path.resolve( __dirname, 'src' );
const DESTINATION = path.resolve( __dirname, 'dist' );

module.exports = {
  mode: 'development',

  entry: './src/index.ts',

  output: {
    path: DESTINATION,
    filename: 'index.bundle.js',
  },

  resolve: {
    extensions: [ '.ts', '.js' ],
    modules: [
      ROOT,
      'node_modules'
    ]
  },

  module: {
      rules: [
          {
            test: /\.tsx?/,
            use: 'ts-loader',
            exclude: /node_modules/,
          },
          {
            test: /\.sass/,
            use: [
              {
                loader: 'style-loader'
              },
              {
                loader: 'css-loader'
              },
              {
                loader: 'postcss-loader'
              },
              {
                loader: 'sass-loader',
                options: {
                  implementation: require('sass')
                }
              }
            ]
          },
          {
            test: /\.png|jpe?g|svg|ico/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  outputPath: 'images'
                }
              }
            ]
          },
          {
            test: /\.woff/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  outputPath: 'assets/fonts'
                }
              }
            ]
          }
      ]
  }
};